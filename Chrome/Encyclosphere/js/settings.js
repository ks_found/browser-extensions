let options = {};
$(document).ready(async function() {
	options = await loadData();

	//$("#aggregatorURLField").val(options.aggregatorURL);

	$("#resultsRange").val(options.resultsCount);

	const range = $('input[type="range"]');

	range.each(function() {
		$(`strong[data-countFor="${$(this).attr("id")}"]`).text($(this).val());
		if ($(this).val() === 1) $(`span[data-textFor="${$(this).attr("id")}"]`).text($(this).attr("data-text"));
		else $(`span[data-textFor="${$(this).attr("id")}"]`).text($(this).attr("data-text") + "s");
	});

	range.on("input", function() {
		$(`strong[data-countFor="${$(this).attr("id")}"]`).text($(this).val());
		if ($(this).val() === 1) $(`span[data-textFor="${$(this).attr("id")}"]`).text($(this).attr("data-text"));
		else $(`span[data-textFor="${$(this).attr("id")}"]`).text($(this).attr("data-text") + "s");
	});

	switch (options.showResults) {
		case "always":
			$("#alwaysShowResultsOption").prop("checked", true);
			break;
		case "ifWikipedia":
			$("#ifWikipediaShowResultsOption").prop("checked", true);
			break;
		case "ifWikipediaTop3":
			$("#ifWikipediaTop3ShowResultsOption").prop("checked", true);
			break;
		case "never":
			$("#neverShowResultsOption").prop("checked", true);
			break;
	}
	$("#resultGetButtonOption").prop("checked", options.resultGetButton);

	$("#duckDuckGoEnabledOption").prop("checked", options.engines.duckDuckGo);
	$("#googleEnabledOption").prop("checked", options.engines.google);

	switch (options.resultLinks) {
		case "originalSource":
			$("#originalSourceLinkOption").prop("checked", true);
			break;
		case "reader":
			$("#readerLinkOption").prop("checked", true);
			break;
		case "encycloSearch":
			$("#encycloSearchLinkOption").prop("checked", true);
			break;
		case "encycloReader":
			$("#encycloReaderLinkOption").prop("checked", true);
			break;
	}

	switch (options.removeWikipedia) {
		case "all":
			$("#allWikipediaRemoveOption").prop("checked", true);
			break;
		case "exactMatches":
			$("#exactMatchWikipediaRemoveOption").prop("checked", true);
			break;
		case "none":
			$("#noWikipediaRemoveOption").prop("checked", true);
			break;
	}

	const wikipediaToReaderOption = $("#wikipediaToReaderOption");
	if (options.resultLinks === "originalSource") {
		options.wikipediaToReader = false;
		wikipediaToReaderOption.prop("disabled", true);
		wikipediaToReaderOption.prop("checked", false);
	} else {
		wikipediaToReaderOption.prop("checked", options.wikipediaToReader);
	}

	$("#noEncyclosphereNoWikipediaRemoveOption").prop("checked", options.noResultsNoRemoveWikipedia);

	switch (options.theme) {
		case "light":
			$("#lightThemeOption").prop("checked", true);
			break;
		case "dark":
			$("#darkThemeOption").prop("checked", true);
			break;
		case "sepia":
			$("#sepiaThemeOption").prop("checked", true);
			break;
		case "system":
			$("#systemThemeOption").prop("checked", true);
			break;
	}

	switch (options.font) {
		case "inter":
			$("#interFontOption").prop("checked", true);
			break;
		case "sourceSans":
			$("#sourceSansFontOption").prop("checked", true);
			break;
		case "baskerville":
			$("#baskervilleFontOption").prop("checked", true);
			break;
		case "system":
			$("#systemFontOption").prop("checked", true);
			break;
	}

	$("input, select").change(function() {
		//options.aggregatorURL = $("#aggregatorURLField").val();
		//if (options.aggregatorURL.endsWith("/")) options.aggregatorURL = options.aggregatorURL.slice(0, -1);

		options.resultsCount = $("#resultsRange").val();

		if ($("#alwaysShowResultsOption").is(":checked")) options.showResults = "always";
		else if ($("#ifWikipediaShowResultsOption").is(":checked")) options.showResults = "ifWikipedia";
		else if ($("#ifWikipediaTop3ShowResultsOption").is(":checked")) options.showResults = "ifWikipediaTop3";
		else if ($("#neverShowResultsOption").is(":checked")) options.showResults = "never";
		options.resultGetButton = $("#resultGetButtonOption").prop("checked");

		options.engines = {};
		options.engines.duckDuckGo = $("#duckDuckGoEnabledOption").prop("checked");
		options.engines.google = $("#googleEnabledOption").prop("checked");

		if ($("#originalSourceLinkOption").is(":checked")) options.resultLinks = "originalSource";
		else if ($("#readerLinkOption").is(":checked")) options.resultLinks = "reader";
		else if ($("#encycloSearchLinkOption").is(":checked")) options.resultLinks = "encycloSearch";
		else if ($("#encycloReaderLinkOption").is(":checked")) options.resultLinks = "encycloReader";

		if ($("#allWikipediaRemoveOption").is(":checked")) options.removeWikipedia = "all";
		else if ($("#exactMatchWikipediaRemoveOption").is(":checked")) options.removeWikipedia = "exactMatches";
		else if ($("#noWikipediaRemoveOption").is(":checked")) options.removeWikipedia = "none";

		if (options.resultLinks === "originalSource") {
			options.wikipediaToReader = false;
			$("#wikipediaToReaderOption").prop("disabled", true);
			$("#wikipediaToReaderOption").prop("checked", false);
		} else {
			if ($("#wikipediaToReaderOption").prop("disabled")) {
				$("#wikipediaToReaderOption").prop("disabled", false);
				$("#wikipediaToReaderOption").prop("checked", true);
			}
			options.wikipediaToReader = $("#wikipediaToReaderOption").prop("checked");
		}

		options.noResultsNoRemoveWikipedia = $("#noEncyclosphereNoWikipediaRemoveOption").prop("checked");

		if ($("#lightThemeOption").is(":checked")) options.theme = "light";
		else if ($("#darkThemeOption").is(":checked")) options.theme = "dark";
		else if ($("#sepiaThemeOption").is(":checked")) options.theme = "sepia";
		else if ($("#systemThemeOption").is(":checked")) options.theme = "system";

		if ($("#interFontOption").is(":checked")) options.font = "inter";
		else if ($("#sourceSansFontOption").is(":checked")) options.font = "sourceSans";
		else if ($("#baskervilleFontOption").is(":checked")) options.font = "baskerville";
		else if ($("#systemFontOption").is(":checked")) options.font = "system";

		chrome.storage.sync.set({options});
	});

	$("#resetBtn").click(function() {
		chrome.storage.sync.remove("options");
		window.location.reload();
	});
});
