class ZWIFile {
    constructor(metadata, files) {
        this.metadata = metadata;
        this.files = files;
    }

    static async fromBlob(blob) {
        if (!(blob instanceof Blob)) throw new Error("Provided object is not a Blob");
        const reader = new zip.ZipReader(new zip.BlobReader(blob));
        const decoder = new TextDecoder();

        let metadata = {};
        let files = {};

        const entries = await reader.getEntries();
        for (let i = 0; i < entries.length; i++) {
            let entry = entries[i];
            const filename = decoder.decode(entry.rawFilename);
            if (filename === "metadata.json") metadata = JSON.parse(await entry.getData(new zip.TextWriter()));
            else files[filename] = await entry.getData(new zip.BlobWriter());
        }
        await reader.close();

        if (metadata === undefined) throw new Error("Metadata not found");

        return new ZWIFile(metadata, files);
    }
}
