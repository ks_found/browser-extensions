async function loadData(noSetTheme) {
	return new Promise((resolve) => {
		chrome.storage.sync.get("options", data => {
			let options = {};
			Object.assign(options, data.options);

			if (Object.entries(options).length === 0) {
				options = {};

				//options.aggregatorURL = "https://encyclosearch.org";

				options.engines = {};
				options.engines.duckDuckGo = true;
				options.engines.google = true;
				options.engines.bing = true;
				options.engines.yahoo = true;
				options.engines.brave = true;
				options.engines.startPage = true;

				options.resultsCount = 3;
				options.showResults = "ifWikipediaTop3";
				options.resultLinks = "reader";
				options.resultGetButton = true;
				options.removeWikipedia = "exactMatches";
				options.wikipediaToReader = true;
				options.noResultsNoRemoveWikipedia = true;
				options.theme = "system";
				options.font = "inter";
			}

			if (options.resultGetButton === undefined) {
				options.resultGetButton = true;
			}

			if (options.axeWikipedia && !options.removeWikipedia) {
				options.removeWikipedia = options.axeWikipedia;
			}

			if (!noSetTheme) {
				const themeStyle = document.querySelector("#themeStyle");
				switch (options.theme) {
					case "light":
						themeStyle.setAttribute("href", "css/themes/light.css");
						break;
					case "dark":
						themeStyle.setAttribute("href", "css/themes/dark.css");
						break;
					case "sepia":
						themeStyle.setAttribute("href", "css/themes/sepia.css");
						break;
					case "system":
						if(window.matchMedia("(prefers-color-scheme: dark)").matches) {
							themeStyle.setAttribute("href", "css/themes/dark.css");
						} else {
							themeStyle.setAttribute("href", "css/themes/light.css");
						}
						break;
				}

				switch (options.font) {
					case "inter":
						document.querySelector("#fontStyle").setAttribute("href", "css/fonts/inter.css");
						break;
					case "sourceSans":
						document.querySelector("#fontStyle").setAttribute("href", "css/fonts/source-sans-pro.css");
						break;
					case "baskerville":
						document.querySelector("#fontStyle").setAttribute("href", "css/fonts/libre-baskerville.css");
						break;
					case "system":
						document.querySelector("#fontStyle").setAttribute("href", "css/fonts/system.css");
						break;
				}
			}
			resolve(options);
		});
	});
}
